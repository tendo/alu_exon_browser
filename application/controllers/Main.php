<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

  public function index($start=1,$span=100)
  {
    $datafile = 'altexeon.list';
    $this->load->model('Mdbas');
    $this->load->library('pagination');
    $this->load->helper('url');

    $config['base_url'] = site_url('Main/index');
    $config['total_rows'] = 2161;//`wc $datafile`;
    $config['per_page'] = $span=2161;
    $this->pagination->initialize($config);

    $res = $this->Mdbas->alternative_exon_list($start,$config['per_page']);

    //$p['content'] = $res;
    //$param["page"] = $this->load->view('template', $p);

    $param['page'] = $res;
    $param['nav'] = $this->pagination->create_links();
    $this->load->view('vlist',$param);
  }

  public function info($start=0,$span=100)
  {
    $this->load->model('Mdbas');

    $res = $this->Mdbas->alternative_exon_list($start,$span);
    $param['content']=$this->$res;
    $this->load->view('vinfo', $param);
  }
}

