<?php

class Mdbas extends CI_Model {
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('table');
  }

  function alternative_exon_list($start, $span)
  {
    $hix = "http://www.h-invitational.jp/hinv/spsoup/locus_view?hix_id=";
    $hit = "http://www.h-invitational.jp/hinv/spsoup/transcript_view?hit_id=";
    $genecard = "http://www.genecards.org/cgi-bin/carddisp.pl?gene=";

    $datafile = 'altexon.list';
    $end = $start + $span - 1;

    $output = array();
    $retval = 0;
    exec("/bin/sed -n $start,${end}p $datafile", $output, $retval);
    $table = array(array('HGNC symbol', 'Genome', 'Transcript', 'chr', 'dir', 'alternative exons'));
    foreach ($output as $line) {
      $line = preg_replace('/(HIX\d+)/',"<a href='$hix$1' target='_r'>$1</a>",$line,1);
      $line = preg_replace('/(HIT\d+)/',"<a href='$hit$1' target='_r'>$1</a>",$line,1);
      $line = preg_replace('/^(\S{2,})/',"<a href='$genecard$1' target='_r'>$1</a>",$line,1);
      $table[] = explode("\t", $line);
    }
    return $this->table->generate($table);
  }
}
