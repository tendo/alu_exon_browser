<html>
<head>
<title>Alternative exon browser</title>
<style type='text/css'>
 table{
  width: 100%;
  border-collapse: collapse;
  font-size: 10pt;
 }
 table th{
  width: 25%;
  padding: 4px;
  text-align: left;
  vertical-align: top;
  color: #333;
  background-color: #eee;
  border: 1px solid #b9b9b9;
 }
 table td{
  padding: 4px;
  background-color: #fff;
  border: 1px solid #b9b9b9;
 }
</style>
</head>
<body>
<?php $nav; ?>
<?php echo $page;?>
<?php $nav; ?>
</body>
</html>